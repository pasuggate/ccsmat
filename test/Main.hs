module Main where

import Test.Framework (defaultMain, testGroup)

import Tests.CCSMat.Sort
import Tests.CCSMat.Vect
import Tests.CCSMat.Algebra
import Tests.CCSMat.Shape


main =
  defaultMain 
  [ testGroup "CCSMat.Sort   " sortTests
  , testGroup "CCSMat.Vect   " vectTests
  , testGroup "CCSMat.Algebra" algTests
  , testGroup "CCSMat.Shape  " shapeTests
  ]
