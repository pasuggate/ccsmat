module Old.UVec
  ( UVec
  , DVec
  , IVec
  , fromOctave
  , permute
  , invperm
  , readVec
  , readIVec
  , chunk
  ) where

import Control.Monad
import Data.Vector.Helpers hiding (DVec, IVec, chunk)
import Data.Vector.Unboxed (Vector, Unbox)
import qualified Data.Vector.Unboxed as Vec

import Data.OctMat (OctMat (OctDense))
import qualified Data.OctMat as Oct


type UVec = Vector
type IVec = Vector Int
type DVec = Vector Double

instance (Unbox a, Num a) => Num (Vector a) where
    (+) = Vec.zipWith (+)
    (-) = Vec.zipWith (-)
    (*) = Vec.zipWith (*)
    fromInteger = Vec.singleton . fromInteger
    signum = Vec.map signum
    abs = Vec.map abs


fromOctave :: OctMat Double -> DVec
fromOctave (OctDense 1 _ v) = v
fromOctave (OctDense _ 1 v) = v
fromOctave _                = error "Only 1xN & Nx1 dense matrices supported."

permute :: Unbox a => Vector a -> IVec -> Vector a
permute v p = bkp v p
{-# INLINE permute #-}

invperm :: IVec -> IVec
invperm p = efn 0 n `bkp` p
  where n = len p
{-# INLINE invperm #-}

readVec :: FilePath -> IO DVec
readVec  = liftM Oct.getRaw . Oct.fromFile
{-# INLINE readVec #-}

------------------------------------------------------------------------------
-- | Reads an Octave vector of indices, subtracting one from each index so
--   that index 0 is the first element, index (n-1) the last.
readIVec :: FilePath -> IO IVec
readIVec  = (Vec.map pred . Oct.getRaw <$>) . Oct.fromFileInt
{-# INLINE readIVec #-}

chunk :: Unbox a => Int -> Vector a -> [Vector a]
chunk n v 
  | n < len v = tak n v:n `chunk` drp n v
  | otherwise = [v]
{-# SPECIALIZE chunk :: Int -> DVec -> [DVec] #-}
