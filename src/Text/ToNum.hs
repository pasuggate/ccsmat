{-# LANGUAGE ForeignFunctionInterface #-}
module Text.ToNum (toDouble, toInt) where

-- import Data.List

-- import System
import Foreign
import Foreign.C.Types
import System.IO.Unsafe
-- import Control.Monad.ST
-- import GHC.Float

import Data.ByteString(ByteString)
import qualified Data.ByteString.Char8 as B
-- import qualified Data.ByteString.Lazy.Char8 as L

--
-- Stolen from:
--      http://www.haskell.org/haskellwiki/Examples/Read_Double
-- read a Double from a lazy ByteString
-- 'read' should be a pure operation, right??
--
toDouble :: ByteString -> Double
toDouble ls = unsafePerformIO $ B.useAsCString ls $ \cstr ->
    realToFrac <$> c_strtod cstr nullPtr

foreign import ccall unsafe "static stdlib.h strtod" c_strtod
    :: Ptr CChar -> Ptr (Ptr CChar) -> IO CDouble


-- TODO: Does this break with negative numbers?
toInt :: ByteString -> Int
toInt xs =
  case B.readInt xs of
    Nothing     -> 0
    Just (n ,_) -> n
{-# INLINE toInt #-}
