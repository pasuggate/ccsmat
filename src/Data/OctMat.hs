module Data.OctMat
       ( OctMat(OctSparse,OctDense)
       , fromBS, fromFile, fromFileInt, toFile
       , subMatrixT, getDims, getRaw
       ) where

-- Octave header format:
-- '# <octave version, data, computer>
-- '# name: <name>'
-- '# type: <matrix|sparse matrix>'
-- '# nnz: <NNZ>' (optional, only for sparse matrices)
-- '# rows: <rows>'
-- '# columns: <cols>'
-- NOTE: Octave stores matrices in row-major mode.
-- NOTE: Only reads files exported using `save -text <fname> <matrix>'.

import Control.Monad
import Data.Functor.Identity (Identity)
import Data.ByteString.Lazy.Char8 (ByteString)
import qualified Data.ByteString.Char8 as S (ByteString, concat)
import qualified Data.ByteString.Lazy.Char8 as C
import Text.Parsec
import Text.Parsec.ByteString.Lazy

import Data.Vector.Unboxed (Vector, Unbox)
import qualified Data.Vector.Unboxed as V

import Text.ToNum


data OctMat a = OctSparse !Int !Int !IVec !IVec !(Vector a)
              | OctDense  !Int !Int !(Vector a)
              deriving  (Eq,Show)

type IVec = Vector Int
type DVec = Vector Double
type OctD = OctMat Double

{-
class OctClass a where
    fromOctave :: OctMat -> a
    toOctave   :: a -> OctMat
-}

--
--  Public functions:
--

-- Use this function to read an Octave matrix from a file string.
fromFile :: FilePath -> IO OctD
fromFile = fromFile_ toDouble

fromFileInt :: FilePath -> IO (OctMat Int)
fromFileInt = fromFile_ toInt

fromFile_ :: (Unbox a, Num a) =>
             (S.ByteString -> a) -> FilePath -> IO (OctMat a)
fromFile_ f fs = do
  mat <- liftM (fromBS f) . C.readFile $! fs
  case mat of
    Nothing -> error $! "Failed to load:\t" ++ fs
    Just m  -> return m

-- Use this function to read an Octave matrix from a ByteString
fromBS :: (Unbox a, Num a) =>
          (S.ByteString -> a) ->
          ByteString -> Maybe (OctMat a)
fromBS f bs =
  case header bs of
    Nothing -> Nothing
    Just (r,c,nz) -> Just $! readmat f r c nz bs

toFile :: (Unbox a, Show a, Num a) =>
          String -> FilePath -> OctMat a -> IO ()
toFile ns fs (OctSparse rn cn rs cs xs) = do
  let h = "# Created by OctMat.hs\n" ++
          "# name: " ++ ns ++ "\n" ++
          "# type: sparse matrix\n" ++
          "# nnz: " ++ show (V.length xs) ++ "\n" ++
          "# rows: " ++ show rn ++ "\n" ++
          "# columns: " ++ show cn ++ "\n"
      h' = C.pack h
      l (r,c,x) = C.pack $! show r ++ " " ++ show c ++ " " ++ show x ++ "\n"
      ls = C.concat . (:) h' . map l . V.toList . V.zip3 rs cs $! xs
  C.writeFile fs ls
toFile ns fs (OctDense rn cn xs) = do
  let h = "# Created by OctMat.hs\n" ++
          "# name: " ++ ns ++ "\n" ++
          "# type: matrix\n" ++
          "# rows: " ++ show rn ++ "\n" ++
          "# columns: " ++ show cn ++ "\n"
      h' = C.pack h
      showrow r ys = concatMap (\p -> ' ':show (ys V.! p)) ps ++ "\n"
        where ps = take cn [r,(r+rn)..]
      ls = C.concat $ h':(map (\r -> C.pack $ showrow r xs) $ [0..(rn-1)])
  C.writeFile fs ls

-- Extract a dense sub-matrx.
-- TODO: Sparse matrix support.
-- TODO: Currently, this returns the transpose.
-- rs - Row Start.
-- rn - Number of elements from Row.
-- cs - Column Start.
-- cn - Number of elements from Column.
subMatrixT :: (Unbox a, Num a) => OctMat a -> (Int, Int, Int, Int) -> OctMat a
subMatrixT (OctSparse _ _ _ _ _) _ = error "subMatrixT: 'OctSparse' unsupported"
subMatrixT (OctDense r _ xs) (rs, rn, cs, cn) = sm
    where
      sm = OctDense rn cn $ V.backpermute xs ix
      ix = V.generate (rn*cn) (\i -> (rs + i `div` rn) * r + cs + i `mod` rn)
{-# SPECIALIZE subMatrixT :: OctD -> (Int,Int,Int,Int) -> OctD #-}

getDims :: (Unbox a, Num a) => OctMat a -> (Int,Int)
getDims (OctDense  r c     _) = (r, c)
getDims (OctSparse r c _ _ _) = (r, c)
{-# SPECIALIZE getDims :: OctD -> (Int,Int) #-}

getRaw :: (Unbox a, Num a) => OctMat a -> Vector a
getRaw (OctDense  _ _    xs) = xs
getRaw (OctSparse _ _ _ _ _) = error "getRaw: 'OctSparse' unsupported"
{-# SPECIALIZE getRaw :: OctD -> DVec #-}


--
--  Private routines for parsing the Octave matrix header.
--
eol :: Monad m => ParsecT ByteString u m ()
eol = do
  skipMany $! noneOf "\n"
  newline
  return ()
{-# INLINE eol #-}

skipComment :: Parser ()
skipComment = do
  char '#'
  eol

isSparse :: Parser Bool
isSparse = do
  string "# type: "
  let sparse' = string "sparse matrix" >> return True
      dense'  = (string "matrix" <|> string "bool matrix") >> return False
  t <- sparse' <|> dense'
  eol
  return t

getNumField :: String -> Parser Int
getNumField s = do
  string s
  n <- getNat
  eol >> return n

getNat :: Monad m => ParsecT ByteString u m Int
getNat = do
  nnz <- many1 digit
  return $! read $! nnz
{-# INLINE getNat #-}

parseHeader :: ParsecT ByteString () Identity (Int, Int, Maybe Int)
parseHeader  = do
  skipComment -- Version
  skipComment -- Name
  t <- isSparse
  nnz <- case t of
    True  -> liftM Just $! getNumField "# nnz: "
    False -> return Nothing
  r <- getNumField "# rows: "
  c <- getNumField "# columns: "
  return $! (r,c,nnz)

header :: ByteString -> Maybe (Int, Int, Maybe Int)
header bs = case parse parseHeader "Octave" bs of
               Left    _ -> Nothing
               Right val -> Just val

readmat :: (Unbox a, Num a) =>
           (S.ByteString -> a) ->
           Int -> Int -> Maybe Int -> ByteString -> OctMat a
readmat f r c nz bs = case nz of
  Nothing -> dense f r c $! droplines 5 bs
  Just nn -> sparse f r c nn $! droplines 6 bs

-- ** Matrix helpers
------------------------------------------------------------------------------
dense :: (Unbox a, Num a) =>
         (S.ByteString -> a) ->
         Int -> Int -> ByteString -> OctMat a
dense f r c bs = OctDense r c v
  where v = V.fromListN (r*c) . map f . stokens $! bs
{-# SPECIALIZE dense :: (S.ByteString -> Double) -> Int -> Int -> ByteString -> OctMat Double #-}

sparse :: (Unbox a, Num a) =>
          (S.ByteString -> a) ->
          Int -> Int -> Int -> ByteString -> OctMat a
sparse f rn cn nz bs = OctSparse rn cn rs cs vs where
  (rs, cs, vs) = V.unzip3 . V.fromListN nz . sparse' . stokens $! bs
  sparse' (r:c:x:xs) = (toInt r, toInt c, f x):sparse' xs
  sparse'         _  = []
{-# SPECIALIZE sparse :: (S.ByteString -> Double) -> Int -> Int -> Int -> ByteString -> OctMat Double #-}

-- ** More helpers
------------------------------------------------------------------------------
-- Convert a lazy ByteString into a list of tokenised strict ByteStrings
stokens :: ByteString -> [S.ByteString]
stokens = map (S.concat . C.toChunks) . C.words

droplines :: Int -> ByteString -> ByteString
droplines n bs = C.unlines . drop n . C.lines $! bs


-- * Unused
------------------------------------------------------------------------------
{-- }
getBody :: Parser String
getBody  = many anyChar
{-# INLINE getBody #-}
--}
