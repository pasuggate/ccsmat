------------------------------------------------------------------------------
-- |
-- Module      : Data.CCSMat
-- Copyright   : (C) Patrick Suggate, 2012
-- License     : BSD3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
--
-- Sparse-Matrix library using the Compressed Column Storage (CCS) format.
--
------------------------------------------------------------------------------

module Data.CCSMat
  ( CCSMat(..)
  , CCSD
    -- ^ Import/Export
  , fromFile
  , toFile
  , toEdges
  , full
    -- ^ Constructors
  , eye
  , zeroes
  , diagFromVector
    -- ^ Queries
  , printCCS
  , getCol
  , getColN
  , getRowN
  , getNNZ
  , getDims
  , rowcounts
    -- ^ Modifications
  , sort
  , transpose
  , unsafeTranspose
    -- ^ Permutations
  , CCSPerm
  , permute
  , symperm
  , sympermT
  , matperm
  , colperm
  , mkperm
  , transposeToPermute
    -- ^ Arithmetic Operations
  , multiply
  , prodShape
  , saxpy
  , simxform
  , add
  , sub
  , csadd
  , csaddR
  , scale
    -- ^ Shape Operations
  , takeDiag
  , dropDiag
    -- ^ Maps
  , xmap
  ) where

import Data.Vector.Storable (Storable)
import Text.Printf

import Data.CCSMat.Internal
import Data.CCSMat.Sort
import Data.CCSMat.File
import Data.CCSMat.Transpose
import Data.CCSMat.Permute
import Data.CCSMat.Multiply
import Data.CCSMat.Addsub

import Data.CCSMat.Shape


printCCS :: (Storable a, PrintfType t) => CCSMat a -> String -> t
printCCS mat name =
  printf "CCSMat: %s [%d x %d], nz = %d, %s.\n" name r c nz sym
  where
    ((r,c), nz) = (getDims mat, getNNZ mat)
    sym = case (isLower mat, isUpper mat) of
      (False, False) -> "up/lo"
      (True , False) -> "lower"
      (False, True ) -> "upper"
      (True , True ) -> "diag"
