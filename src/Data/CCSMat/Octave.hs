module Data.CCSMat.Octave
  ( fromOctave
  , toOctave
  , readOct
  ) where

import Control.Monad.ST
import qualified Data.Vector.Storable as V
import qualified Data.Vector.Unboxed  as U

import Data.CCSMat.Internal
import Data.CCSMat.Sort
import Data.OctMat (OctMat(OctDense,OctSparse))
import qualified Data.OctMat as Oct


-- Error messages:
errSparseOnly :: [Char]
errSparseOnly  = "Only sparse matrix formats are supported by CCSMat"

fromOctave :: OctMat Double -> CCSD
fromOctave (OctDense _ _ _) = error errSparseOnly
fromOctave (OctSparse rn cn rs cs xs) = runST $ do
  let ri  = V.convert $ U.map (\x -> x-1) rs -- Octave uses [1..N] for arrays
      ci  = V.convert $ U.map (\x -> x-1) cs -- Octave uses [1..N] for arrays
      nnz = U.length xs
  m <- sortIntoCols rn cn nnz ri ci (V.convert xs)
  sortM m

toOctave :: CCSMat Double -> OctMat Double
toOctave m@(CCS rn ri cp xs) = OctSparse rn cn rs cs (U.convert xs)
    where cn = getColN m
          rs = U.convert $ V.map (+1) ri -- Convert back to [1..N] arrays
          cs = U.convert $ V.map (+1) (p2i cp)

readOct :: FilePath -> IO CCSD
readOct  = fmap fromOctave . Oct.fromFile
