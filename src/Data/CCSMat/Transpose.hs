{-# LANGUAGE BangPatterns, TupleSections #-}
module Data.CCSMat.Transpose
       ( unsafeTranspose
       , transpose
       , rowcounts
       ) where

import GHC.Types (SPEC(..))
import Control.Monad.Primitive
import Control.Monad.ST
import Data.Vector.Helpers
import Data.Vector.Storable (Vector, Storable)
import qualified Data.Vector.Storable as Vec

import Data.CCSMat.Internal
import Data.CCSMat.Sort


-- * Transposition functions
------------------------------------------------------------------------------
-- | Perform a transpose without first sort/checking the ordering of elements
--   in each column.
--   
--   NOTE: This funtion does not preserve column ordering.
--   TODO: Rewrite, this is currently just a clone of the C code.
--   
unsafeTranspose :: Storable a => CCSMat a -> CCSMat a
unsafeTranspose (CCS rn ri cp xs) = CCS (cc-1) ri' cp' xs'
  where
    nnz = len ri
    rc  = rowcounts' ri rn
    cp' = Vec.scanl' (+) 0 rc
    cc  = len cp
    (ri', xs') = runST $ do
      xw <- Vec.thaw cp'
      xi <- new nnz
      xx <- new nnz
      let go !_ !j
            | j'  <  cc = col SPEC j (cp!j') (cp!j) >> go SPEC j'
            | otherwise = return ()
            where j' = j+1
          col !_ !j !q !p
            | p  <  q   = upd j (ri!p) (xs!p) >> col SPEC j q (p+1)
            | otherwise = return ()
          {-# INLINE upd #-}
          upd j i x = rd xw i >>= \q -> wr xi q j >> wr xx q x >> wr xw i (q+1)
      go SPEC 0
      (,) <$> frz xi <*> frz xx
{-# SPECIALIZE unsafeTranspose :: CCSMat R -> CCSMat R #-}

transpose :: Storable a => CCSMat a -> CCSMat a
transpose m = runST $ (sortM . unsafeTranspose) m
{-# SPECIALIZE transpose :: CCSMat R -> CCSMat R #-}

{-- }
inplaceTranspose :: Storable a => CCSMat a -> CCSMat a
{-# SPECIALIZE inplaceTranspose :: CCSD -> CCSD #-}
inplaceTranspose (CCS rn ri cp xs) = CCS (cc-1) ri' cp' xs'
  where
    rc  = rowcounts ri rn
    cp' = Vec.scanl' (+) 0 rc
    cc  = len cp
    (ri', xs') = runST $ do
      xw <- Vec.thaw cp'
      xi <- thw ri
      xx <- thw xs
      let go !_ !j
            | j'  <  cc = col SPEC j (cp!j') (cp!j) >> go SPEC j'
            | otherwise = return ()
            where j' = j+1
          col !_ !j !q !p
            | p  <  q   = upd j (ri!p) (xs!p) >> col SPEC j q (p+1)
            | otherwise = return ()
          {-# INLINE upd #-}
          upd j i x = rd xw i >>= \q -> wr xi q j >> wr xx q x >> wr xw i (q+1)
      go SPEC 0
      (,) <$> frz xi <*> frz xx
--}


-- * Helper functions
------------------------------------------------------------------------------
-- | Count the number of elements within each row of the sparse-matrix.
--   
rowcounts :: CCSMat a -> Vector Z
rowcounts (CCS rn ri _ _) = rowcounts' ri rn


-- * Private Functions
------------------------------------------------------------------------------
-- | Count the number of elements within each row of the sparse-matrix.
--   
rowcounts' :: IVec -> Int -> IVec
rowcounts' ri m = runST $ do
  rc <- thw $ m `rep` 0
  let incM :: PrimMonad m => MVec m Int -> Int -> m ()
      incM v i = rd v i >>= wr v i . (+1)
      {-# INLINE incM #-}
  Vec.mapM_ (incM rc) ri
  frz rc
{-# INLINE rowcounts' #-}
