{-# LANGUAGE BangPatterns, FlexibleContexts, Rank2Types #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.CCSMat.Sort
-- Copyright   : (C) Patrick Suggate, 2011
-- License     : BSD3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Sparse-matrix sorting functions.
-- 
-- Architecture:
-- Uses mutable-arrays to build a permutation vector, and then performs a
-- back-permute to yield the sorted arrays, of the (CCS) sparse-matrix.
-- 
-- Changelog:
--  + ??/??/2011  --  initial file;
--  + 20/08/2015  --  refactored and updated;
-- 
-- TODO:
--  + testbenches, testing, and optimisation;
--  + can I reduce the dependencies?
--  + find a better home for `makePermute`?
-- 
------------------------------------------------------------------------------

module Data.CCSMat.Sort
  ( sort
  , sortM
  , makePermute
  ) where

import GHC.Types (SPEC(..))
import Control.Monad.ST
import Control.Monad.Primitive
import Data.Vector.Helpers
import Data.Vector.Storable (Vector, Storable)
import qualified Data.Vector.Storable         as Vec
import qualified Data.Vector.Algorithms.Intro as Mut

import Data.CCSMat.Internal


-- * Sparse-matrix sorting functions.
------------------------------------------------------------------------------
-- | Sort the row-indices of a sparse-matrix, and using the `ST`-monad so that
--   mutable arrays can be used for building the permutation vector.
sort :: Storable t => CCSMat t -> CCSMat t
sort m = runST $ sortM m
-- {-# SPECIALIZE sort :: CCSD -> CCSD #-}
{-# INLINE [1] sort #-}

-- | Sort the row-indices of a sparse-matrix, and allowing any primitive monad
--   to be used, so that mutable arrays can be used for building the
--   permutation vector.
sortM :: (PrimMonad m, Storable a) => CCSMat a -> m (CCSMat a)
sortM (CCS rn ri cp xs) = do
  pp <- makePermute ri cp
  let ri' = bkp ri pp
      xs' = bkp xs pp
  return $ CCS rn ri' cp xs'
{-# INLINE [1] sortM #-}


-- * Helper functions.
------------------------------------------------------------------------------
-- | Make a permutation vector; i.e., a vector of indices to use for
--   permuting some other vector (or matrix).
--   
--   TODO: Test and optimise. Should I use inlining, or specialisation?
--   FIXME: Cannot use both `unsafeThaw` and `unsafeFreeze`?
makePermute ::
     Ord a
  => PrimMonad m
  => Storable a
  => Vector a
  -> Vector Z
  -> m (Vector Z)
makePermute ri cp = do
  rp <- Vec.thaw $ 0 `efn` len ri
  let sortp = Mut.sortByBounds cmp rp
      {-# INLINE [0] sortp #-}
      cmp i j = compare (ri!i) (ri!j)
      {-# INLINE [0] cmp #-}
      sortj !_ !j !p
        | p  <  nz  = let q  = cp!j'
                          j' = j+1
                      in  sortp p q >> sortj SPEC j' q
        | otherwise = return ()
      nz = len ri
  sortj SPEC 0 0
  frz rp
{-# INLINE [1] makePermute #-}
