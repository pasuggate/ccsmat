{-# LANGUAGE FlexibleContexts #-}
module Data.CCSMat.Shape {-# DEPRECATED "Needs to be replaced." #-}
       ( isLower, isUpper, isSymmetric, isPermDiag
       , triu, tril
       , matUnzip
       ) where

-- NOTE: Obsolete.

import Control.Monad.ST
import Data.Vector.Helpers
import Data.Vector.Storable(Vector, Storable)
import qualified Data.Vector.Unboxed  as U
import qualified Data.Vector.Storable as Vec

import Data.CCSMat.Internal
import Data.CCSMat.Transpose


-- TODO: When using the `i2p` function, the testbench fails?
tril :: (Storable a, U.Unbox a) => CCSMat a -> CCSMat a
tril (CCS rn ri cp xs) = CCS rn (Vec.convert ri') cp' (Vec.convert xs')
  where ci = U.convert $ p2i cp
        ix = U.zip3 (U.convert ri) ci (U.convert xs)
        (ri',ci',xs') = U.unzip3 $ U.filter (\(i,j,_) -> i >= j) ix
--         cp' = i2p (Vec.convert ci') (-1) 0
        cp' = packCols (len cp - 1) (Vec.convert ci')
{-# SPECIALIZE tril :: CCSMat R -> CCSMat R #-}

-- TODO: This is almost identical to the above, can the duplication be fixed?
triu :: (Storable a, U.Unbox a) => CCSMat a -> CCSMat a
triu (CCS rn ri cp xs) = CCS rn (Vec.convert ri') cp' (Vec.convert xs')
  where ci = U.convert $ p2i cp
        ix = U.zip3 (U.convert ri) ci (U.convert xs)
        (ri',ci',xs') = U.unzip3 $ U.filter (\(i,j,_) -> i <= j) ix
--         cp' = i2p (Vec.convert ci') (-1) 0
        cp' = packCols (len cp - 1) (Vec.convert ci')
{-# SPECIALIZE triu :: CCSMat R -> CCSMat R #-}

matUnzip :: (Storable a, U.Unbox a) => CCSMat a -> U.Vector (Int,Int,a)
matUnzip (CCS _ ri cp xs) = U.zip3 (U.convert ri) ci (U.convert xs)
  where ci = U.convert $ p2i cp

------------------------------------------------------------------------------
isUpper, isLower :: Storable a => CCSMat a -> Bool
{-# SPECIALIZE isLower :: CCSMat R -> Bool #-}
{-# SPECIALIZE isUpper :: CCSMat R -> Bool #-}
isLower m = colTest m (\j -> Vec.all (>=j) . fst) $ getColN m - 1
isUpper m = colTest m (\j -> Vec.all (<=j) . fst) $ getColN m - 1
-- isLower m = colTest m (\j (ix,_) -> Vec.all (>=j) ix) $! getColN m - 1
-- isUpper m = colTest m (\j (ix,_) -> Vec.all (<=j) ix) $! getColN m - 1

-- TODO: More robust checking.
-- TODO: This is very slow.
isSymmetric :: (Storable a, Ord a, Fractional a) =>
               CCSMat a -> Bool
isSymmetric m@(CCS rn _ cp xs) = rn == cn && res/nnz < eps
    where (CCS _ _ _ xs') = transpose m
          cn  = len cp - 1
          nnz = fromIntegral . len $ xs
          res = Vec.sum . Vec.map (\x -> x*x) . Vec.zipWith (-) xs $ xs'
          eps = 1e-6
{-# SPECIALIZE isSymmetric :: CCSMat R -> Bool #-}

isPermDiag :: CCSMat a -> (Bool, Bool)
isPermDiag (CCS rn ri cp _) = (p, d) where
  n  = len cp - 1
  ns = efn 0 n
  s  = rn == n && cp!n == n && ns == nit cp
  d  = s       && ns == ri
  p  = runST $ do
    let testRow fs rs
          | rs == Vec.empty = return True
          | otherwise     = do
              let r = hed rs
              b <- rd fs r
              case b of
                True  -> return False
                False -> do
                  wr fs r True
                  testRow fs (tal rs)
    flg <- Vec.thaw $ rep n False
    testRow flg ri

------------------------------------------------------------------------------
-- Perform a test (yielding a Bool) on each element of the vector.
colTest ::
     Storable a
  => CCSMat a
  -> (Int -> (Vector Z, Vector a) -> Bool)
  -> Int
  -> Bool
colTest m f j
  | j < 0     = True
  | otherwise = l && colTest m f (j-1)
  where l = f j $ getCol m j
{-# SPECIALIZE colTest :: CCSMat R -> (Int -> (Vector Z, Vector R) -> Bool) -> Int -> Bool #-}
