{-# LANGUAGE BangPatterns #-}
module Data.CCSMat.Permute
  ( CCSPerm
  , permute
  , symperm
  , sympermT
  , matperm
  , colperm
  , mkperm
  , transposeToPermute
  ) where

import Control.Monad.ST
import Data.Vector.Helpers
import Data.Vector.Storable(Vector, Storable)
import qualified Data.Vector.Storable as Vec
import qualified Data.Vector.Generic as G

import Data.CCSMat.Internal
import Data.CCSMat.Transpose


type CCSPerm = CCSMat Int


-- TODO:
permute :: Storable a => CCSMat a -> Vector Z -> CCSMat a
permute _ _ = error $ modname ++ ".Permute.permute:22: Undefined."

-- For a symmetric matrix A, return just the permuted version of the upper
-- triangular part of A.
-- NOTE: This function takes the inverse-permutation vector, `pinv'.
symperm :: Storable a => CCSMat a -> Vector Z -> CCSMat a
symperm a@(CCS rn ri cp xs) pinv = runST $ do
  let cn = getColN a
      js = efn 0 cn
  -- Create the column pointers:
  cp' <- thw $ rep cn 0
  Vec.forM_ js $ \j -> do
    let ps = slicePtrs cp j
        j' = pinv!j
    Vec.forM_ ps $ \p -> do
      let i  = ri!p
      if i > j
        then return ()
        else do let p' = max i' j'
                    i' = pinv!i
                c <- rd cp' p'
                wr cp' p' (c+1)
  cc <- Vec.scanl' (+) 0 <$> frz cp'
  cw <- Vec.thaw cc

  -- Perform the permutation:
  ci <- new (lst cc)
  cx <- new (lst cc)
  Vec.forM_ js $ \j -> do
    let ps = slicePtrs cp j
        j' = pinv!j
    Vec.forM_ ps $ \p -> do
      let i  = ri!p
          i' = pinv!i
      if i > j
        then return ()
        else do let p' = max i' j'
                q <- rd cw p'
                wr cw p' (q+1) >> wr ci q (min i' j') >> wr cx q (xs!p)
  flip (CCS rn) cc <$> frz ci <*> frz cx
{-# SPECIALIZE symperm :: CCSD -> Vector Z -> CCSD #-}

-- For a symmetric matrix A, return just the permuted version of the lower
-- triangular part of A.
-- NOTE: This function takes the inverse-permutation vector, `pinv'.
sympermT :: Storable a => CCSMat a -> Vector Z -> CCSMat a
sympermT a@(CCS rn ri cp xs) pinv = runST $ do
  let cn = getColN a
      js = efn 0 cn
  -- Create the column pointers:
  cp' <- thw $ rep cn 0
  Vec.forM_ js $ \j -> do
    let ps = slicePtrs cp j
        j' = pinv!j
    Vec.forM_ ps $ \p -> do
      let i  = ri!p
      if i < j
        then return ()
        else do let p' = min i' j'
                    i' = pinv!i
                c <- rd cp' p'
                wr cp' p' (c+1)
  cc <- Vec.scanl' (+) 0 <$> frz cp'
  cw <- Vec.thaw cc

  -- Perform the permutation:
  ci <- new (lst cc)
  cx <- new (lst cc)
  Vec.forM_ js $ \j -> do
    let ps = slicePtrs cp j
        j' = pinv!j
    Vec.forM_ ps $ \p -> do
      let i  = ri!p
          i' = pinv!i
      if i < j
        then return ()
        else do let p' = min i' j'
                q <- rd cw p'
                wr cw p' (q+1) >> wr ci q (max i' j') >> wr cx q (xs!p)
  flip (CCS rn) cc <$> frz ci <*> frz cx
{-# SPECIALIZE sympermT :: CCSD -> Vector Z -> CCSD #-}


-- * Build permutation matrices from index vectors
------------------------------------------------------------------------------
-- | Construct a permutation matrix from the given local-to-global vector of
--   indices.
mkperm :: (Storable a, Num a) => Z -> Vector Z -> CCSMat a
mkperm rn ri = CCS rn ri cp xs where
  xs = len ri `rep` 1
  cp = efn 0 (len ri+1)
{-# INLINE mkperm #-}


-- * Weird permutations
------------------------------------------------------------------------------
-- | To use this, precomputed row-indices, column-pointers, and a permutation
--   vector are required.
matperm :: Storable a => CCSMat a -> CCSPerm -> CCSMat a
matperm (CCS _ _ _ xs) (CCS rn ri cp p) = CCS rn ri cp xs'
  where xs' = bkp xs p
{-# SPECIALIZE matperm :: CCSD -> CCSPerm -> CCSD #-}

-- | Permute each column, using the supplied permute vector for all columns.
--   NOTE: Output is unsorted.
--   TODO: Untested, is this a forward or reverse permutation?
colperm :: CCSMat a -> Vector Z -> CCSMat a
colperm (CCS rn ri cp xs) p = CCS rn ri' cp xs
  where ri' = Vec.map (p!) ri
{-# INLINE colperm #-}

transposeToPermute :: CCSMat a -> CCSPerm
transposeToPermute (CCS rn ri cp _) = transpose (CCS rn ri cp p)
    where p = 0 `efn` len ri
