{-# LANGUAGE BangPatterns, FlexibleContexts, TypeFamilies #-}
module Data.CCSMat.Addsub
       ( add
       , sub
       , csadd
       , csadd'
       , csaddR
       ) where

import GHC.Types (SPEC(..))
import Control.Monad.ST
import Data.Bool
import Data.VectorSpace
import Data.Vector.Helpers
import Data.Vector.Storable(Storable)
import qualified Data.Vector.Storable as V
-- import qualified Data.Vector.Storable.Mutable as M

-- import Data.Vector.Helpers
import Data.CCSMat.Internal


-- * VectorSpace orphan instances
------------------------------------------------------------------------------
instance (Storable a, AdditiveGroup a) => AdditiveGroup (CCSMat a) where
--   zeroV = CCS 0 V.empty V.empty V.empty
  zeroV                     = CCS 0 V.empty (V.singleton 0) V.empty
  negateV (CCS rn ri cp xs) = CCS rn ri cp $ V.map negateV xs
  CCS 0 _ _ _ ^+^ b = b
  a ^+^ CCS 0 _ _ _ = a
  a ^+^ b = add a b
  CCS 0 _ _ _ ^-^ b = negateV b
  a ^-^ CCS 0 _ _ _ = a
  a ^-^ b = a ^+^ negateV b
--   a ^-^ b = CCS.sub a b
  {-# INLINE zeroV #-}
  {-# INLINE negateV #-}
  {-# INLINE (^+^) #-}
  {-# INLINE (^-^) #-}

instance (VectorSpace a, Storable a) => VectorSpace (CCSMat a) where
-- instance (Scalar a ~ a, Num a, Storable a) => VectorSpace (CCSMat a) where
--   type Scalar (CCSMat a) = a
  type Scalar (CCSMat a) = Scalar a
--   (*^) = CCS.scale
  s *^ CCS rn ri cp xs = CCS rn ri cp $ V.map (s*^) xs
  {-# INLINE (*^) #-}


-- * Exported add & sub operations.
------------------------------------------------------------------------------
add :: (Storable a, AdditiveGroup a) => CCSMat a -> CCSMat a -> CCSMat a
-- {-# SPECIALIZE add :: CCSD -> CCSD -> CCSD #-}
{-# INLINE add #-}
add  = csadd'

-- NOTE: Too slow, don't use this. The constants seem to be preventing
--   specialisation from working correctly?
sub :: (Num a, Storable a) => CCSMat a -> CCSMat a -> CCSMat a
-- {-# SPECIALIZE sub :: CCSD -> CCSD -> CCSD #-}
{-# INLINE sub #-}
sub a b = csadd a b 1 (-1)

------------------------------------------------------------------------------
-- | Scale A & B by constants, then add.
--   TODO: This is very, very slow?
csadd :: (Storable a, Num a) => CCSMat a -> CCSMat a -> a -> a -> CCSMat a
{-# SPECIALIZE csadd :: CCSD -> CCSD -> Double -> Double -> CCSD #-}
csadd (CCS an ai ap ax) (CCS bn bi bp bx) alpha beta
  | an /= bn  = error "ERROR: Matrix dimensions do not match."
  | otherwise = runST $ do
    let cnz   = len ax + len bx
        acnt  = len ap
    cx  <- new cnz
    ci  <- new cnz
    cp  <- new acnt
    flg <- thw $ V.replicate an (-1)
    let {-# INLINE upd #-}
        upd c i x = wr flg i c >> wr ci c i >> wr cx c x >> return (c+1)
        ago !q !p !c
          | p  <  q   = upd c (ai!p) (alpha*(ax!p)) >>= ago q (p+1)
          | otherwise = return c
        bgo !s !q !p !c
          | p  <  q   = rd flg i >>= \t ->
            bool (upd c i y) (rd cx t >>= wr cx t . (+y) >> return c) (t>=s) >>=
              bgo s q (p+1)
          | otherwise = return c
          where i = bi!p
                y = beta*(bx!p)
        col !j !c
          | j' < acnt =
            wr cp j c >> ago (ap!j') (ap!j) c >>= -- Scatter A.
            bgo c (bp!j') (bp!j) >>= col j' -- Scatter and add B.
          | otherwise = wr cp j c >> return c
          where j' = j+1
    nz  <- col 0 0
    CCS an <$> tak nz `fmap` frz ci <*> frz cp <*> tak nz `fmap` frz cx

------------------------------------------------------------------------------
-- | Scale A & B by constants, then add.
--   TODO: Quirky specialisations?
csadd' :: (Storable a, AdditiveGroup a) => CCSMat a -> CCSMat a -> CCSMat a
{-# SPECIALIZE csadd' :: CCSD -> CCSD -> CCSD #-}
-- {-# INLINE [1] csadd' #-}
csadd' (CCS an ai ap ax) (CCS bn bi bp bx)
  | an /= bn  = error "ERROR: Matrix dimensions do not match."
  | otherwise = runST $ do
    let cnz   = len ax + len bx
        acnt  = len ap
    cx  <- new cnz
    ci  <- new cnz
    cp  <- new acnt
    flg <- thw $ V.replicate an (-1)
    let {-# INLINE upd #-}
        upd c i x = wr flg i c >> wr ci c i >> wr cx c x >> return (c+1)
        ago !_ !q !p !c
          | p  <  q   = upd c (ai!p) (ax!p) >>= ago SPEC q (p+1)
          | otherwise = return c
        bgo !_ !s !q !p !c
          | p  <  q   = rd flg i >>= \t -> do
            bool (upd c i y) (rd cx t >>= wr cx t . (^+^y) >> return c) (t>=s) >>=
              bgo SPEC s q (p+1)
          | otherwise = return c
          where i = bi!p
                y = bx!p
        col !_ !j !c
          | j' < acnt =
            wr cp j c >> ago SPEC (ap!j') (ap!j) c >>= -- Scatter A.
            bgo SPEC c (bp!j') (bp!j) >>= col SPEC j' -- Scatter and add B.
          | otherwise = wr cp j c >> return c
          where j' = j+1
    nz  <- col SPEC 0 0
    CCS an <$> tak nz `fmap` frz ci <*> frz cp <*> tak nz `fmap` frz cx


-- * OBSOLETE:
------------------------------------------------------------------------------
-- | Add.
--   TODO: This is ugly.
csaddR :: (Storable a, AdditiveGroup a) => CCSMat a -> CCSMat a -> CCSMat a
{-# SPECIALIZE csaddR :: CCSD -> CCSD -> CCSD #-}
csaddR (CCS an ai ap ax) (CCS bn bi bp bx)
  | an /= bn  = error "ERROR: Matrix dimensions do not match."
  | otherwise = runST $ do
    let acol' = len ap
        acols = acol' - 1
        anz   = len ax
        bnz   = len bx
    cx  <- new (anz+bnz)
    ci  <- new (anz+bnz)
    cp  <- new acol'
    flg <- thw (rep an (-1) :: IVec)
    wr cp 0 0
    let addcols !aq !bq !j !ak !aw !bk !bw
          | j == acols = return ()
          | otherwise  = do
            pcstart <- rd cp j
            let np = (hed aq') - a
                nq = (hed bq') - b
                a   = hed aq
                aq' = tal aq
                b   = hed bq
                bq' = tal bq
                j' = j+1
                cscatter !ri xs fx wi wx !n !nz
                  | n == 0    = return (nz,ri,xs)
                  | otherwise = do
                    let i   = hed ri
                        ri' = tal ri
                        x   = hed xs
                        xs' = tal xs
                        n'  = n-1
                    f <- rd fx i
                    if f < pcstart then
                      do wr fx i nz
                         wr wx nz x
                         wr wi nz i
                         cscatter ri' xs' fx wi wx n' (nz+1)
                      else do
                         y <- rd wx f
                         wr wx f (x^+^y)
                         cscatter ri' xs' fx wi wx n' nz
            (nzp,ak',aw') <- cscatter ak aw flg ci cx np pcstart
            (nzq,bk',bw') <- cscatter bk bw flg ci cx nq nzp
            wr cp j' nzq
            addcols aq' bq' j' ak' aw' bk' bw'
    addcols ap bp 0 ai ax bi bx
    nnz <- rd cp acols
    cp' <- frz cp
    ri  <- frz (mlc 0 nnz ci)
    xs  <- frz (mlc 0 nnz cx)
    return $! CCS an ri cp' xs
