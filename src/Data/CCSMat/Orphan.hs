
------------------------------------------------------------------------------
-- |
-- Module      : Data.CCSMat.Orphan
-- Copyright   : (C) Patrick Suggate 2017
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
-- 
-- Orphan instances for `CCSMat's.
--
-- NOTE:
-- 
-- Changelog:
--  + 20/05/2017  -- initial file (refactored from root);
-- 
-- TODO:
-- 
------------------------------------------------------------------------------

module Data.CCSMat.Orphan where

import Data.VectorSpace
import Data.Vector.Storable (Storable)
import qualified Data.Vector.Storable as V

import Data.CCSMat.Internal
import Data.CCSMat.Multiply
import Data.CCSMat.Addsub


{-- }
------------------------------------------------------------------------------
-- | Define some algebraic operations.
--   
--   TODO: Specialise for doubles is possible?
--   TODO: Orphan instance. I need to make an instance of `Data.VectorSpace`
--     for `CCSMat a`?
instance (Storable a, Num a) => Num (CCSMat a) where
  (+) = csaddR
  (-) = sub
  (*) = multiply
  abs    (CCS rn ri cp xs) = CCS rn ri cp $ V.map Prelude.abs xs
  signum (CCS rn ri cp xs) = CCS rn ri cp $ V.map Prelude.signum xs
  negate (CCS rn ri cp xs) = CCS rn ri cp $ V.map negate xs
  fromInteger = eye . fromInteger

{-
convert :: (Storable a, Storable b, Num a, Num b) =>
           (a -> b) -> CCSMat a -> CCSMat b
convert f (CCS rn ri cp xs) = CCS rn ri cp xs'
  where xs' = V.map f xs
-}

instance (Storable a, AdditiveGroup a) => AdditiveGroup (CCSMat a) where
  zeroV                     = CCS 0 V.empty (V.singleton 0) V.empty
  negateV (CCS rn ri cp xs) = CCS rn ri cp $ V.map negateV xs
  (^+^)                     = csaddR
--}
