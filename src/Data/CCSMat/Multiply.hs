{-# LANGUAGE BangPatterns, TupleSections #-}
module Data.CCSMat.Multiply
       ( multiply
       , prodShapeMat
       , prodShape
       , saxpy
         -- ^ composite operations:
       , simxform
         -- ^ broken?
       , fmul
       , mulTest
       ) where

-- FIXME: Not robust, better checking of input-matrix shapes is needed.

-- TODO: Currently `saxpy' mutates it's 3rd argument, the `shape' matrix.
--   A cleaner way of reusing shapes needs to be implemented.
-- TODO: Sparse-matrix multiplication, for matrices of the same shape each 
--   time, should be very quick when using appropriate accleleration data-
--   structures.

import GHC.Types (SPEC(..))
import Control.Monad.ST
import Control.Monad.Primitive
import Data.Bool
import Data.Vector.Helpers
import Data.Vector.Storable (Vector, Storable)
import qualified Data.Vector.Storable as Vec

import Data.CCSMat.Internal


------------------------------------------------------------------------------
-- | Matrix multiplication, translated from Tim Davis' C implementation, and
--   it uses imperative programming, and mutable data-structures.
--   
multiply :: (Num a, Storable a) => CCSMat a -> CCSMat a -> CCSMat a
multiply a@(CCS _ _ ap _) b@(CCS bn _ _ _) 
  | acols /= bn = error msg
  | otherwise   = saxpy a b c
  where c     = prodShapeMat a b
        acols = len ap - 1
        msg   = "Multiply.hs: ERROR: Matrix dimensions do not match!"
{-# SPECIALIZE multiply :: CCSD -> CCSD -> CCSD #-}

------------------------------------------------------------------------------
-- | Algorithm: For each column `j', do: 
--     C(:,j) := A(:,k) * B(k,j)
--   
--   NOTE: The multiplication is in-place, writing into the `cc` matrix, and
--     this must have the correct shape.
--   
--   TODO: Worth optimising for special cases? (E.g, perhaps for diagonal- and
--     permutation- matrices.)
--   
saxpy :: (Num a, Storable a) => CCSMat a -> CCSMat a -> CCSMat a -> CCSMat a
saxpy (CCS an ar ac ax) (CCS _ br bc bx) cc@(CCS _ ci cp cx) = runST $ do
  rr  <- Vec.unsafeThaw ci
  xr  <- Vec.unsafeThaw cx
  flg <- thw (rep an (-1) :: Vector Z)
  let ccnt = len cp
      {-# INLINE set #-}
      set i x c = wr flg i c >> wr rr c i >> wr xr c x
      {-# INLINE upd #-}
      upd s i x c = rd flg i >>= \r ->
        bool (set i x c >> return (c+1))
        (rd xr r >>= wr xr r . (+x) >> return c) (r >= s)
      fscl !_ !bkj !qend !q !cnz
        | q  < qend = set (ar!q) (ax!q*bkj) cnz >>
                      fscl SPEC bkj qend (q+1) (cnz+1)
        | otherwise = return cnz
      go !_ !j !cnz
        | j' < ccnt = bool (go SPEC j' cnz)
                      (fscl SPEC (bx!p) (ac!(k+1)) (ac!k) cnz >>=
                       bcol SPEC cnz pend (p+1) >>= go SPEC j') (p<pend)
        | otherwise = return cc
        where j'    = j+1
              k     = br!p
              p     = bc!j
              pend  = bc!j'
      bcol !_ !s !pend !p !cnz
        | p  < pend = acol SPEC s (bx!p) (ac!(k+1)) (ac!k) cnz >>=
                      bcol SPEC s pend (p+1)
        | otherwise = return cnz
        where k = br!p
      acol !_ !s !bkj !qend !q !cnz
        | q  < qend = upd s (ar!q) (ax!q*bkj) cnz >>=
                      acol SPEC s bkj qend (q+1)
        | otherwise = return cnz
  go SPEC 0 0
{-# SPECIALIZE saxpy :: CCSD -> CCSD -> CCSD -> CCSD #-}


-- ** Product-shape algorithms, "inspired" by Tim Davis' SuiteSparse.
------------------------------------------------------------------------------
-- | Compute the product's "shape-matrix," but leave the entries undefined,
--   for now.
--   
prodShapeMat :: Storable a => CCSMat a -> CCSMat a -> CCSMat a
prodShapeMat aa bb = runST $ do
  cp <- fst <$> prodShapeM aa bb
  rr <- new (lst cp)
  xr <- new (lst cp)
  CCS (getRowN aa) `flip` cp <$> frz rr <*> frz xr
{-# SPECIALIZE prodShapeMat :: CCSD -> CCSD -> CCSD #-}

prodShape :: CCSMat a -> CCSMat a -> Vector Z
prodShape aa bb = runST $ fst <$> prodShapeM aa bb

------------------------------------------------------------------------------
-- | A copy of the shape calculation routine for `ssmult_saxpy' by Tim Davis.
--     http://www.cise.ufl.edu/research/sparse/ssmult/SSMULT/ssmult_saxpy.c
--   Description of the algorithm:
--   
--   For each non-zero element in each column of B, determine whether there is
--   a non-zero element in the corresponding row of A.
--
prodShapeM :: PrimMonad m => CCSMat a -> CCSMat a -> m (Vector Z, MVec m Int)
prodShapeM (CCS an ar ac _) (CCS _ br bc _) = do
  let bcnt  = len bc
  cc  <- new bcnt
  flg <- thw (rep an (-1) :: Vector Z)
  let go !j !cnz
        | j' < bcnt = wr cc j cnz >> bcol j (bc!j') (bc!j) cnz >>= go j'
        | otherwise = wr cc j cnz >> frz cc
        where j' = j+1
      bcol !j !pend !p !cnz
        | p  < pend = acol j (ac!(k+1)) (ac!k) cnz >>= bcol j pend (p+1)
        | otherwise = return cnz
        where k = br!p
      acol !j !qend !q !cnz
        | q  < qend = upd j (ar!q) cnz >>= acol j qend (q+1)
        | otherwise = return cnz
      {-# INLINE [0] upd #-}
      upd j i c = rd flg i >>=
                  bool (const (c+1) <$> wr flg i j) (return c) . (==j)
  (,flg) <$> go 0 0
{-# INLINE [1] prodShapeM #-}


-- * Composite multiplications
------------------------------------------------------------------------------
-- | "Similarity transform" of the `b` matrix, by the `a` matrices.
--   
--   TODO: look at the matrix dimensions and nz info, to estimate the fastest
--     association?
--   
simxform :: (Storable a, Num a) => CCSMat a -> CCSMat a -> CCSMat a -> CCSMat a
-- simxform :: CCSMat R -> CCSMat R -> CCSMat R -> CCSMat R
simxform a' b a = a' `multiply` (b `multiply` a)
{-# INLINE[2] simxform #-}


-- * EXPERIMENTAL.
------------------------------------------------------------------------------
-- | Algorithm: For each column `j', do: 
--     C(:,j) := A(:,k) * B(k,j)
--   
--   FIXME: Doesn't work (when tested with non-square matrices).
--   
fmul :: (Num a, Storable a) => CCSMat a -> CCSMat a -> CCSMat a
fmul aa@(CCS an ar ac ax) bb@(CCS _ br bc bx) = runST $ do
  (cp, flg) <- prodShapeM aa bb
  let nz   = lst cp
      ccnt = len cp
  rr  <- new nz
  xr  <- new nz
  let set i x c = wr flg i c >> wr rr c i >> wr xr c x >> return (c+1)
      {-# INLINE set #-}
      upd s i x c = rd flg i >>= \r ->
        bool (set i x c)
--         (rd xr r >>= wr xr r . (+x) >> return c) (r >= s)
        (rd xr r >>= wr xr r . (+x) >> return c) (r < s)
      {-# INLINE upd #-}
      go !_ !j !cnz
        | j' < ccnt = bcol SPEC cnz (bc!j') (bc!j) cnz >>= go SPEC j'
        | otherwise = CCS an `flip` cp <$> frz rr <*> frz xr
        where j'    = j+1
      bcol !_ !s !pend !p !cnz
        | p  < pend = acol SPEC s (bx!p) (ac!(k+1)) (ac!k) cnz >>=
                      bcol SPEC s pend (p+1)
        | otherwise = return cnz
        where k     = br!p
      acol !_ !s !bkj !qend !q !cnz
        | q  < qend = upd s (ar!q) (ax!q*bkj) cnz >>=
                      acol SPEC s bkj qend (q+1)
        | otherwise = return cnz
  go SPEC 0 0
{-# SPECIALIZE fmul :: CCSD -> CCSD -> CCSD #-}


-- * Testing & examples.
------------------------------------------------------------------------------
mulTest :: IO ()
mulTest  = do
  let ii = eye 10 :: CCSD
  print   ii
  print $ ii `fmul` ii
