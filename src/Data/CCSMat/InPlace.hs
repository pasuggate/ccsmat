module Data.CCSMat.InPlace (colUpdateM) where

import Control.Monad.ST
import Data.Vector.Helpers
import Data.Vector.Storable(Vector, Storable)
import Data.CCSMat.Internal


-- NOTE: Obsolete.
colUpdateM :: Storable a => CCSMat a -> Int -> Vector a -> CCSMat a
colUpdateM m@(CCS _ _ cp xs) j v = runST $ do
  let xj = slc (cp!j) (len v) xs
  x' <- thw xj
  vcp x' v
  frz x'
  return m
{-# SPECIALIZE colUpdateM :: CCSMat R -> Int -> Vector R -> CCSMat R #-}
