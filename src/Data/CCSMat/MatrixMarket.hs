module Data.CCSMat.MatrixMarket (readMMBody) where

------------------------------------------------------------------------------
--
--  Convert to/from MatrixMarket files.
--

import Data.ByteString.Char8 (ByteString)
import qualified Data.ByteString.Char8 as C
import Control.Monad.Primitive

import Text.ToNum
import Data.Vector.Helpers


-- * MatrixMarket header information.
------------------------------------------------------------------------------
data Header  = Header Content Storage Element Shape
             deriving Eq

data Content = Matrix | Vector
             deriving Eq

data Storage = Array  | Coord
             deriving Eq

data Element = Reals  | Complex | Integers | Pattern
             deriving Eq

data Shape   = General
             | SelfAdjoint | SkewAdjoint
             | Symmetric   | SkewSymmetric
             deriving Eq


instance Show Header where
  show (Header c s e x) =
    "%%MatrixMarket  " ++ show c ++ show s ++ show e ++ show x ++ "\n"

instance Show Content where
  show Matrix        = "matrix  "
  show Vector        = "vector  "

instance Show Storage where
  show Array         = "array       "
  show Coord         = "coordinate  "

instance Show Element where
  show Reals         = "real    "
  show Complex       = "complex "
  show Integers      = "integer "
  show Pattern       = "pattern "

instance Show Shape where
  show General       = "general         "
  show SelfAdjoint   = "Hermitian       "
  show SkewAdjoint   = "skew-Hermitian  "  -- TODO: Supported?
  show Symmetric     = "symmetric       "
  show SkewSymmetric = "skew-symmetric  "


{-- }
------------------------------------------------------------------------------
-- | Each supported element data-type needs a separate instance to encode, and 
--   decode, the MatrixMarket headers and elements.
class MatrixMarket a where
  toMatrixMarket   :: CCSMat a -> String
  fromMatrixMarket :: String -> CCSMat a

class MatrixMarketHeader a where
  buildHeader :: CCSMat a -> Header


-- TODO:
instance MatrixMarket Double where
  toMatrixMarket   = undefined
  fromMatrixMarket = undefined

instance MatrixMarketHeader Double where
  buildHeader _ = Header Matrix Coord Reals General


------------------------------------------------------------------------------
-- | MatrixMarket Sparse Format:
--   %%MatrixMarket <type> <format> <elements> <shape>
--   <rows> <cols> <nnz>
--   <row>  <col>  <val>
--   <row>  <col>  <val>
--   ...
--   
--   TODO: Header.
readMMHeader :: ByteString -> Header
readMMHeader = undefined
--}


------------------------------------------------------------------------------
-- | Read just the body of a MatrixMarket file.
readMMBody :: PrimMonad m =>
              ByteString -> m (Int, Int, Int, IVec, IVec, DVec)
readMMBody bs = do
  let ls = C.lines bs
      [rn, cn, nnz] = map toInt . C.words $ head ls
  ri <- new nnz
  ci <- new nnz
  xs <- new nnz
  let fill _ []     = return ()
      fill p (l:ms) = do
        let [r, c, x] = C.words l
        wr ri p (toInt r) >> wr ci p (toInt c)
        wr xs p (toDouble x) >> fill (p+1) ms
  fill 0 (tail ls)
  ri' <- frz ri
  ci' <- frz ci
  xs' <- frz xs
  return (rn, cn, nnz, ri', ci', xs')
