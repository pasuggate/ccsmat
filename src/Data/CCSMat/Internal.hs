{-# LANGUAGE CPP, BangPatterns, FlexibleContexts, DeriveGeneric
  #-}

------------------------------------------------------------------------------
-- |
-- Module      : Data.CCSMat.Element
-- Copyright   : (C) Patrick Suggate 2017
-- License     : GPL3
-- 
-- Maintainer  : Patrick Suggate <patrick.suggate@gmail.com>
-- Stability   : Experimental
-- Portability : non-portable
--
-- Internal representation data-types for Compressed-Column Sparse (CCS)
-- matrices.
-- 
-- Changelog:
--  + ??/??/2009  --  initial file;
-- 
-- TODO:
--  + parameterise by vector/array type?
-- 
------------------------------------------------------------------------------

module Data.CCSMat.Internal
       ( -- module Data.CCSMat.Internal
--        , module Data.Vector.Helpers
         -- data types:
         CCSMat (..)
       , CCSD
       , CCSF
       , CCS1
         -- constructors:
       , diagFromVector
       , fromTriplet
       , eye
       , zeroes
         -- element-wise functions:
       , hasElem
       , elemAt
       , elemIndex
         -- accessors:
       , getCol
       , colIdxs
       , colIdxs'
       , getColN
       , getRowN
       , getNNZ
       , getDims
         -- conversions:
       , toEdges
       , full
       , fullUsing
         -- modifiers:
       , scale
       , xmap
       , takeDiag
       , dropDiag
         -- helpers:
       , slicePtrs
       , p2i
       , p2j -- ^ better and faster version of 'p2i'
       , sortIntoCols
       , packCols
       , packColsM
       , unpack
       , unscanlSum
       , modname
       ) where

import GHC.Types (SPEC(..))
import GHC.Generics (Generic, Generic1)
import Control.Monad.ST
import Control.Monad.Primitive
import Control.DeepSeq
import Data.Aeson
import Text.Printf

import Data.Vector.Helpers
import Data.Vector.Storable (Vector, Storable)
import qualified Data.Vector.Storable as V
import qualified Data.Vector.Storable.Mutable as M
import qualified Data.Vector.Generic as G


-- * CCS data-types.
------------------------------------------------------------------------------
-- | The `CCSMat` data-type stores compressed columns using the standard
--   representation; i.e. the CSC/CCS sub-topic within:
--   
--     http://en.wikipedia.org/wiki/Sparse_matrix
--   
data CCSMat a = CCS !Int !(Vector Z) !(Vector Z) !(Vector a)
              deriving (Eq, Generic, Generic1)

type CCSD = CCSMat Double
type CCSF = CCSMat Float
type CCS1 = CCSMat ()

instance (Storable a, ToJSON   a) => ToJSON   (CCSMat a)
instance (Storable a, FromJSON a) => FromJSON (CCSMat a)


-- * Instances for some standard type-classes.
------------------------------------------------------------------------------
instance (Show a, Storable a) => Show (CCSMat a) where
  show (CCS rn ri cp xs) = "ri:\t" ++ ri' ++ "\ncp:\t" ++ cp' ++ "\nxs:\t" ++ xs'
    where ri' = (rn'++) . show $ V.toList ri
          cp' = (cn'++) . show $ V.toList cp
          xs' = (nn'++) . show $ V.toList xs
          rn' = printf "(r=%d)" rn
          cn' = printf "(c=%d)" (len cp-1)
          nn' = printf "(n=%d)" (len xs)

instance NFData a => NFData (CCSMat a)
-- instance NFData1 CCSMat

modname :: String
modname  = "Data.CCSMat"


-- * Constructors.
------------------------------------------------------------------------------
-- | Build a sparse, diagonal matrix from the given vector.
--   O(n)
{-# INLINE diagFromVector #-}
diagFromVector :: Storable a => Vector a -> CCSMat a
diagFromVector v = CCS n (V.enumFromN 0 n) (V.enumFromN 0 (n+1)) v
  where n = len v

fromTriplet :: Storable a => Z -> Z -> Z -> [(Z, Z, a)] -> CCSMat a
{-# SPECIALIZE fromTriplet :: Z -> Z -> Z -> [(Z, Z, R)] -> CCSD #-}
fromTriplet rn cn nz ts =
  let (is, js, xs) = unzip3 ts
  in  runST $ sortIntoCols rn cn nz (vec is) (vec js) (vec xs)

------------------------------------------------------------------------------
-- | Build a sparse, identity matrix.
eye :: (Num a, Storable a) => Int -> CCSMat a
{-# INLINE eye #-}
eye  = diagFromVector . flip V.replicate 1

-- | Build a sparse-matrix, for the given dimensions, and with zero for every
--   entry.
zeroes :: Storable a => Int -> Int -> CCSMat a
{-# INLINE zeroes #-}
zeroes m n = CCS m V.empty (succ n `rep` 0) V.empty


-- * Element-wise functions.
------------------------------------------------------------------------------
scale :: (Num a, Storable a) => a -> CCSMat a -> CCSMat a
{-# INLINE scale #-}
scale s (CCS rn ri cp xs) = CCS rn ri cp (V.map (s*) xs)


-- * Accessors:
------------------------------------------------------------------------------
-- TODO: This is horrible.
-- TODO: Implement a `SparseVec' type.
getCol :: Storable a => CCSMat a -> Int -> (Vector Z, Vector a)
{-# SPECIALIZE getCol :: CCSD -> Int -> (Vector Z, DVec) #-}
getCol (CCS _ ri cp xs) j = (slc p n ri, slc p n xs)
    where p   = cp!j
          n   = cp!(j+1) - p

------------------------------------------------------------------------------
-- | Get the row-indices, for the given column index.
colIdxs :: CCSMat a -> Int -> Vector Z
{-# INLINE colIdxs #-}
colIdxs (CCS _ ri cp _) = colIdxs' ri cp

colIdxs' :: Vector Z -> Vector Z -> Int -> Vector Z
{-# INLINE colIdxs' #-}
colIdxs' ri cp j = slc p (cp!(j+1) - p) ri
  where p = cp!j

-- Matrix dimension N (only for NxN matrix with elements along its diagonal).
{-# INLINABLE getColN #-}
{-# INLINABLE getRowN #-}
{-# INLINABLE getNNZ  #-}
getColN, getRowN, getNNZ :: CCSMat a -> Int
getColN (CCS _ _ cp _) = len cp - 1
getRowN (CCS rn _ _ _) = rn
getNNZ  (CCS _ _ cp _) = lst cp

{-# INLINABLE getDims #-}
getDims :: CCSMat a -> (Int,Int)
getDims (CCS rn _ cp _) = (rn, len cp-1)

-- Return a slice of pointers to all non-zero elements of A.
-- For iterative algorithms, it is often necessary to iterate over all
-- non-zero elements within a given row.
slicePtrs :: Vector Z -> Int -> Vector Z
{-# INLINE slicePtrs #-}
slicePtrs cp k = G.enumFromN s (e-s)
  where (s,e) = (cp!k, cp!(k+1))


------------------------------------------------------------------------------
-- | Construct a vector of edges, corresponding to the non-zero entries of
--   the given sparse matrix.
--   O(m)
toEdges :: CCSMat a -> [(Int,Int)]
toEdges (CCS _ ri cp _) = zip (V.toList $ p2i cp) $ V.toList ri

------------------------------------------------------------------------------
-- | Convert column-pointers into column-indices that can be paired with the 
--   vector of row-indices.
p2i :: Vector Z -> Vector Z
{-# RULES "p2i/p2j" p2i = p2j #-}
{-# INLINE [1] p2i #-}
p2i cp = runST $ do
  let nnz = lst cp
  ci <- new nnz
  let fill p np ps j
        | p == nnz  = return ()
        | p == np   = fill np (hed ps) (tal ps) (j+1)
        | otherwise = wr ci p j >> fill (p+1) np ps j
      cp' = tal cp
  fill 0 (hed cp') (tal cp') 0
  frz ci

-- NOTE: Faster version of the above.
p2j :: Vector Z -> Vector Z
{-# INLINE [1] p2j #-}
p2j cp = runST $ do
  let nnz = lst cp
  ci <- new nnz
  let go !p !q !j
        | p  >= nnz = return ()
        | p  ==  q  = go p (cp!(j+2)) (j+1)
        | otherwise = wr ci p j >> go (p+1) q j
  go 0 (cp!1) 0
  frz ci

sortIntoCols ::
  (PrimMonad m, Storable a) =>
  Int -> Int -> Int -> Vector Z -> Vector Z -> Vector a -> m (CCSMat a)
{-# SPECIALIZE sortIntoCols ::
      Int -> Int -> Int -> Vector Z -> Vector Z -> DVec -> IO CCSD #-}
sortIntoCols rn cn nnz ri ci xs = do
  cp  <- packColsM cn ci
  cp' <- V.thaw cp
  ri' <- new nnz
  xs' <- new nnz
  let sortc !_ !k
        | k  <  nnz = do
          let c = ci!k
          p <- rd cp' c
          wr ri' p (ri!k) >> wr xs' p (xs!k) >> wr cp' c (p+1)
          sortc SPEC (k+1)
        | otherwise = return ()
  sortc SPEC 0
  flip (CCS rn) cp <$> frz ri' <*> frz xs'

------------------------------------------------------------------------------
-- | Safe version for packing column-indices, as it can work with unsorted
--   indices.
packCols :: Int -> Vector Z -> Vector Z
packCols cn ci = runST $ packColsM cn ci

packColsM :: PrimMonad m => Int -> Vector Z -> m IVec
{-# INLINE [1] packColsM #-}
packColsM cn ci = do
  cp <- new (cn+1)
  let cnt !p = whn (0 < p) (mfy cp (1+) (ci!p') >> cnt p')
        where p' = p-1
      go  !p !q = rd cp p >>= \c -> wr cp p q >> whn (p < cn) (go (p+1) (q+c))
  M.set cp 0 >> cnt (len ci) >> go 0 0 >> frz cp

{-# DEPRECATED unsafePackCols, i2p "These are slower and less-safe than `packCols`" #-}
------------------------------------------------------------------------------
-- | Convert column-indices to column-pointers.
--   USAGE: cp = unsafePackCols cn ci
--   
--   TODO: This seems to be O(N).
unsafePackCols :: Int -> Vector Z -> Vector Z
unsafePackCols n js =
  let m = len js
      go !i !p | p  >=  m  = Just (m, (i , m ))
               | i  ==  j  = go i p'
               | i' ==  j  = Just (p, (i', p'))
               | otherwise = Just (p, (i', p ))
        where j  = js!p
              i' = i+1
              p' = p+1
  in  V.unfoldrN (n+1) (uncurry go) (-1, 0)

-- | Convert column-indices to column-pointers.
--   USAGE: cp = i2p ci (-1) 0
--   OBSOLETE: The above version is faster and (slightly) safer.
i2p :: Vector Z -> Int -> Int -> Vector Z
i2p ci !j !p
  | V.null ci = V.singleton p  -- TODO: Should be `V.null ci'
  | x == j'   = p `V.cons` i2p ci' j' p' -- Test to see if matrix has
  | x /= j    = p `V.cons` i2p ci  j' p  -- any all-zero columns.
  | otherwise = i2p ci' x p'
  where ci' = tal ci
        x   = hed ci
        j'  = j+1
        p'  = p+1

------------------------------------------------------------------------------
-- | Convert a sparse matrix to a dense vector, of (zero and non-zero)
--   elements.
--   NOTE: Column-major ordering.
full :: (Num a, Storable a) => CCSMat a -> Vector a
full  = fullUsing 0
{-# SPECIALIZE full :: CCSD -> DVec #-}
{-# SPECIALIZE full :: CCSF -> FVec #-}
{-# INLINABLE  full #-}

-- | Convert a sparse matrix to a dense vector, of (zero and non-zero)
--   elements.
fullUsing :: Storable a => a -> CCSMat a -> Vector a
fullUsing z (CCS rn ri cp xs) =
  let co = unpack rn cp
      rp = G.zipWith (+) ri co
      nn = rn * (len cp - 1)
      zz = rep nn z
  in  upd_ zz rp xs
{-# INLINE fullUsing #-}

-- | Constructs a list of pointers to the beginning of each column, for each
--   of the row-indices/data-elements.
unpack :: Int -> Vector Z -> Vector Z
unpack s ps =
  let qs = unscanlSum ps
      js = G.enumFromN 0 (len qs)
  in  G.concatMap (\j -> rep (qs!j) (s*j)) js
{-# INLINE unpack #-}

-- | Reverse a list of sums, computed using `scanl`.
unscanlSum :: (Num a, Storable a) => Vector a -> Vector a
unscanlSum ps = G.zipWith (-) (tal ps) ps
{-# INLINE unscanlSum #-}


-- * Functions to modify `CCSMat` elements.
------------------------------------------------------------------------------
xmap :: (Storable a, Storable b) => (a -> b) -> CCSMat a -> CCSMat b
xmap f (CCS rn ri cp xs) = CCS rn ri cp $ G.map f xs
{-# INLINE [1] xmap #-}


-- * Element indexing functionality.
------------------------------------------------------------------------------
hasElem :: CCSMat a -> Z -> Z -> Bool
hasElem (CCS _ ri cp _) i j = 
  let ix = slc p (cp!(j+1) - p) ri
      p  = cp!j
  in  G.elem i ix

elemAt :: Storable a => CCSMat a -> Z -> Z -> Maybe a
elemAt (CCS _ ri cp xs) i j =
  let ix = slc p n ri
      p  = cp!j
      n  = cp!(j+1) - p
  in  G.elemIndex i ix >>= \k -> Just (xs!(p+k))

elemIndex :: CCSMat a -> Z -> Z -> Maybe Z
elemIndex (CCS _ ri cp _) i j =
  let ix = slc p (cp!(j+1) - p) ri
      p  = cp!j
  in  (p+) <$> G.elemIndex i ix


-- * Extraction of (off-)diagonal entries.
------------------------------------------------------------------------------
-- TODO: Make a more efficient implementation?
takeDiag :: (Storable a, Num a) => CCSMat a -> Vector a
takeDiag mat@(CCS rn _ cp _) =
  let nn = max rn (len cp - 1)
  in  gen nn (\i -> maybe 0 id $ elemAt mat i i)

dropDiag :: Storable a => CCSMat a -> CCSMat a
dropDiag mat@(CCS rn ri cp xs) = runST $ do
  let nn = max rn (cc-1)
      cc = len cp
      dn = foldl (\s i -> maybe s (const (s+1)) $ elemIndex mat i i) 0 [0..nn-1]
      nz = len xs - dn
  rr <- new nz
  cr <- new cc
  xr <- new nz
  let cols j q
        | j  < cc-1 = rows j q >>= \q' -> wr cr (j+1) q' >> cols (j+1) q'
        | otherwise = return q
      rows j q = let p = cp!j in copy j (cp!succ j - p) p q
      copy _ 0 _ q = return q
      copy j c p q
        | i  ==  j  = copy j c' p' q
        | otherwise = wr rr q (ri!p) >> wr xr q (xs!p) >> copy j c' p' (q+1)
        where
          (c', p', i) = (c-1, p+1, ri!p)
  wr cr 0 0 >> cols 0 0
  CCS rn <$> frz rr <*> frz cr <*> frz xr
