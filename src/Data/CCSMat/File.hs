module Data.CCSMat.File
       ( fromFile
       , toFile
       ) where

------------------------------------------------------------------------------
--
--  Read and write sparse-matrices using a file-format that is similar to the
--  matrix-market format (but without the header).
--

import qualified Data.ByteString.Char8 as C
import Data.Vector.Storable (Storable)
import qualified Data.Vector.Storable as V

import Data.CCSMat.Internal
import Data.CCSMat.Sort
import Data.CCSMat.MatrixMarket


fromFile :: FilePath -> IO (CCSMat Double)
fromFile fs = do
  bs <- C.readFile fs
  (rn, cn, nnz, ri, ci, xs) <- readMMBody bs
  m  <- sortIntoCols rn cn nnz ri ci xs
  sortM m

toFile :: (Storable a, Show a) => FilePath -> CCSMat a -> IO ()
toFile fs m@(CCS rn ri cp xs) = do
  let ls = concatMap showLine $ zip3 (V.toList ri) (V.toList ci) (V.toList xs)
      showLine (r,c,x) = show r ++ " " ++ show c ++ " " ++ show x ++ "\n"
      ci = p2i cp
      hdr = showLine (rn, getColN m, V.length xs)
  Prelude.writeFile fs $ hdr ++ ls
