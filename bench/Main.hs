{-# LANGUAGE BangPatterns #-}
module Main where

import System.IO.Unsafe
import Data.List (scanl')
import Control.Monad.ST
import Control.Monad.IO.Class
import qualified Data.Vector.Storable as Vec
import qualified Data.Vector.Generic.Mutable as M
import Criterion.Main (defaultMain, bench, bgroup, whnf)

import Data.CCSMat.Internal
import Data.CCSMat.Vector
import Data.CCSMat.Transpose
import Data.CCSMat.Sort
import Data.CCSMat.Multiply
import qualified Data.CCSMat.OldMult as Old
import Data.CCSMat.Addsub
import qualified Data.CCSMat.File as CCS
import qualified Data.CCSMat.Pipes as CCS
import qualified Data.CCSMat.Shape as CCS
import qualified Data.CCSMat.Stream as CCS
import qualified Data.CCSMat.Bundle as CCS
import Data.Vector.Sparse

import Data.Matrix.CholMod (CM, CMSparse, CMDense, CMFactor)
import qualified Data.Matrix.CholMod as CM
import Data.Matrix.CSparse
import Data.CCSMat.SuiteSparse.CSparse
import qualified Data.Matrix.CSparse.CSparsePtr as CP


i2p' cn ci = i2p ci (-1) 0
{-# INLINE matsum #-}
matsum (CCS _ _ _ xs) = Vec.sum xs
{-# INLINE matsuM #-}
matsuM (CSparse _ _ _ _ _ xs _) = Vec.sum xs
-- {-# INLINE matSUM #-}
matSUM cs = cmsSum cs
{-# INLINE vecsum #-}
vecsum (SparseVector _ _ xs) = Vec.sum xs
cmsSum :: MonadIO m => CMSparse -> CM m Double
{-# INLINE cmsSum #-}
cmsSum  = fmap Vec.sum . CM.getSparseVals

rcs (CCS _ ri cp _) = rowcounts ri (Vec.length cp - 1)
rcS (CCS _ ri cp _) = CCS.rowcountS ri (Vec.length cp - 1)

{-# INLINE matstream #-}
matstream mm@(CCS rn _ cp _) = runST $ CCS.fromColS rn cc (CCS.colS mm)
  where cc = Vec.length cp

{-# INLINE [1] prodshapeMat #-}
prodshapeMat x y = let (CCS _ _ cp _) = prodShapeMat x y in cp

ccs2cmsD :: MonadIO m => CCSMat Double -> CM m CMSparse
ccs2cmsD (CCS rn ri cp xs) = do
  let cn = Vec.length cp - 1
      nz = Vec.length ri
--   cs <- CM.allocSparse rn cn nz False False CM.cholmod_s_unsym CM.cholmod_real
  cs <- CM.allocSparse rn cn nz False True CM.cholmod_s_unsym CM.cholmod_real
  CM.setSparseRows ri cs
  CM.setSparseCols cp cs
--   CM.setSparseCnts cp cs
  CM.setSparseVals xs cs
  CM.setSparseDType CM.cholmod_double cs
  CM.setSparseSorted False cs
  return cs

ccs2cspD :: CCSMat Double -> IO CP.CSparse
ccs2cspD (CCS rn ri cp xs) = do
  cs <- CP.cs'spalloc rn (len cp-1) (len ri)
  CP.cs'setCols cp cs
  CP.cs'setRows ri cs
  CP.cs'setVals xs cs
  return cs

solveGSM :: MonadIO m => CMSparse -> CMDense -> CM m CMDense
{-# INLINE solveGSM #-}
solveGSM matA bvec = do
  matL <- CM.analyse matA
  CM.factorise matA matL
  xvec <- CM.solve matL bvec
  return xvec

solveL :: MonadIO m => CMFactor -> CMDense -> CMSparse -> CM m CMDense
{-# INLINE solveL #-}
solveL matL bvec matA = do
  CM.factorise matA matL
  CM.solve matL bvec

solveSp :: MonadIO m => CMFactor -> CMSparse -> CMSparse -> CM m CMSparse
{-# INLINE solveSp #-}
solveSp matL bvec matA = do
  CM.factorise matA matL
  CM.solveSparse matL bvec


-- * Benchmarking groups, for various matrix operation families.
------------------------------------------------------------------------------
linbench cm aX lX bX fX =
  bgroup "Linear solvers"
  [ bench "cmSolve                " $ whnf (flip cmSolve             bX) aX
  , bench "cmSolveL               " $ whnf (cmSolveL              lX bX) aX
  , bench "cmSolveSp              " $ whnf (cmSolveSp             lX fX) aX
  ]
  where
    cmSolve matA bvec = CM.unsafeStepCholMod (solveGSM matA bvec) cm
    cmSolveL matL bvec matA =
      CM.unsafeStepCholMod (solveL matL bvec matA) cm
    cmSolveSp matL bvec matA =
      CM.unsafeStepCholMod (solveSp matL bvec matA) cm


matbench mm cm mX =
  let mM = ccs2csD mm
      mP = unsafePerformIO (ccs2cspD mm)
      cpTrans = unsafePerformIO . CP.cs_spfree . CP.cs'transpose
      cmTranspose cs = CM.unsafeStepCholMod (CM.transpose cs >>= matSUM) cm
      cmTril cs = CM.unsafeStepCholMod (CM.symlower cs >>= matSUM) cm
      cmTriu cs = CM.unsafeStepCholMod (CM.symupper cs >>= matSUM) cm
  in  mP `seq` bgroup "Matrix reshaping"
      [ bench "unsafeTranspose    " $ whnf (matsum . unsafeTranspose   ) mm
--   , bench "matstream          " $ whnf (matsum . matstream         ) mm
--   , bench "toFromCols         " $ whnf (matsum . fromSparseCols . toSparseCols) mm
--       , bench "transpose          " $ whnf (matsum . transpose         ) mm
--       , bench "cs'transpose       " $ whnf (matsuM . flip cs'transpose True) mM
--       , bench "CP.cs'transpose    " $ whnf (cpTrans) mP
      , bench "cmTranspose        " $ whnf (cmTranspose                ) mX
--   , bench "CCS.trans          " $ whnf (matsum . CCS.trans         ) mm
--       , bench "CCS.tranS          " $ whnf (matsum . CCS.tranS         ) mm
      , bench "cmTril             " $ whnf (cmTril                     ) mX
      , bench "cmTriu             " $ whnf (cmTriu                     ) mX
--       , bench "CCS.trilF          " $ whnf (matsum . CCS.trilF         ) mm
--       , bench "CCS.trilS          " $ whnf (matsum . CCS.trilS         ) mm
--       , bench "CCS.trilP          " $ whnf (matsum . CCS.trilP         ) mm
--       , bench "Shape.tril         " $ whnf (matsum . CCS.tril          ) mm
--   , bench "Shape.triu         " $ whnf (matsum . CCS.triu          ) mm
--   , bench "CCS.triuS          " $ whnf (matsum . CCS.triuS         ) mm
--   , bench "rowcounts          " $ whnf (Vec.sum . rcs         ) mm
--   , bench "rowcountS          " $ whnf (Vec.sum . rcS     ) mm
--   , bench "isLower            " $ whnf (CCS.isLower                ) mm
--   , bench "isLowerS           " $ whnf (CCS.isLowerS               ) mm
  ]


mulbench aa bb cm aX bX = 
  let aA = ccs2csD aa
      bB = ccs2csD bb
      aP = unsafePerformIO (ccs2cspD aa)
      bP = unsafePerformIO (ccs2cspD bb)
      cpMul x = unsafePerformIO . CP.cs_spfree . CP.cs'multiply x
--       {-# INLINE cmMul #-}
      cmMul x y = CM.unsafeStepCholMod (CM.mulSparse x y >>= matSUM) cm
  in  aX `seq` bX `seq` bgroup "Multiplication"
      [ bench "cs'multiply        " $ whnf (matsuM . cs'multiply     aA) bB
--       , bench "cs'multiply (2)    " $ whnf (matsuM . cs'multiply     bB) aA
--       , bench "cpMul              " $ whnf (cpMul                    aP) bP
--       , bench "cpMul (2)          " $ whnf (cpMul                    bP) aP
      , bench "cmMul              " $ whnf (cmMul                    aX) bX
      , bench "cmMul (2)          " $ whnf (cmMul                    bX) aX
--       , bench "multiply           " $ whnf (matsum . multiply        aa) bb
--       , bench "multiply (2)       " $ whnf (matsum . multiply        bb) aa
--   , bench "multiply   (sorted)" $ whnf (matsum . sort . multiply aa) bb
--       , bench "fmul               " $ whnf (matsum . fmul        aa) bb
--       , bench "fmul (2)           " $ whnf (matsum . fmul        bb) aa
--   , bench "prodShapeMat       " $ whnf (Vec.sum . prodshapeMat   aa) bb
--   , bench "prodShape          " $ whnf (Vec.sum . prodShape      aa) bb
--   [ bench "prodShape          " $ whnf (Vec.sum . prodshape      aa) bb
--   , bench "prodShapeS         " $ whnf (Vec.sum . CCS.prodShapE  aa) bb
--   , bench "mulS               " $ whnf (matsum . CCS.mulS        aa) bb
--   , bench "mulS (2)           " $ whnf (matsum . CCS.mulS        bb) aa
--   , bench "Old.multiply       " $ whnf (matsum . Old.multiply    aa) bb
--   , bench "Old.multiply (2)   " $ whnf (matsum . Old.multiply    bb) aa
--   , bench "mulSlow            " $ whnf (matsum . mulSlow         aa) bb
--   , bench "mul                " $ whnf (matsum . mul             aa) bb
--   , bench "mulMap             " $ whnf (matsum . mulMap          aa) bb
--       , bench "mulSpMV            " $ whnf (matsum . mulSpMV         aa) bb
--       , bench "mulSpMV (2)        " $ whnf (matsum . mulSpMV         bb) aa
--   , bench "unsafeMul          " $ whnf (matsum . unsafeMul       aa) bb
--   , bench "unsafeMul (2)      " $ whnf (matsum . unsafeMul       bb) aa
      ]


addbench aa bb cm aX bX =
  let aA = ccs2csD aa
      bB = ccs2csD bb
      aP = unsafePerformIO (ccs2cspD aa)
      bP = unsafePerformIO (ccs2cspD bb)
      cpAdd x = unsafePerformIO . CP.cs_spfree . CP.cs'add x
--       cm = CM.unsafeCommon
--       aX = CM.unsafeStepCholMod (ccs2cmsD aa) cm
--       bX = CM.unsafeStepCholMod (ccs2cmsD aa) cm
--       {-# INLINE cmAdd #-}
      cmAdd x y = CM.unsafeStepCholMod (CM.addSparse x y >>= matSUM) cm
  in  aX `seq` bX `seq` bgroup "Addition/subtraction"
      [ bench "cs'add             " $ whnf (matsuM . cs'add     aA bB 1) 1
      , bench "cmAdd              " $ whnf (cmAdd                    aX) bX
--       , bench "cpAdd              " $ whnf (cpAdd                    aP) bP
--       , bench "add                " $ whnf (matsum . add             aa) bb
--       , bench "csaddR             " $ whnf (matsum . csaddR          aa) bb
--       , bench "sub                " $ whnf (matsum . sub             aa) bb
--       , bench "uncurry add        " $ whnf (matsum . uncurry add       ) (aa,bb)
--       , bench "csadd 1 1          " $ whnf (matsum . csadd      aa bb 1) 1
      ]


vecbench ux vx =
  bgroup "Vector operations"
  [ bench "vecAdd             " $ whnf (vecsum . vecAdd          ux) vx
  , bench "vecAddMap          " $ whnf (vecsum . vecAddMap       ux) vx
  , bench "csVecAdd           " $ whnf (vecsum . csVecAdd        ux) vx
  , bench "unsafeVecAdd       " $ whnf (vecsum . unsafeVecAdd    ux) vx
  ]


-- * Testing and benchmarking.
------------------------------------------------------------------------------
cmTest :: CM IO Int
-- cmTest :: CM IO CMSparse
cmTest  = do
  matA   <- CM.readSparse "/home/patrick/Dropbox/phd/data/eit625_A.mtx"
  CM.printSparse matA "A  "
  matA' <- CM.transpose matA
  CM.printSparse matA' "A' "
  matW1_ <- liftIO $ CCS.fromFile "/home/patrick/Dropbox/phd/data/eit625_W1.mtx"
  matW1  <- ccs2cmsD matW1_
  CM.printSparse matW1 "W1 "
--   matW1  <- CM.readSparse "/home/patrick/Dropbox/phd/data/eit625_W1.mtx"
  matW2  <- CM.readSparse "/home/patrick/Dropbox/phd/data/eit625_W2.mtx"
  CM.printSparse matW2 "W2 "
  matW2' <- CM.transpose matW2
  CM.printSparse matW2' "W2'"
  matW3  <- CM.readSparse "/home/patrick/Dropbox/phd/data/eit625_W3.mtx"
  CM.printSparse matW3 "W3 "
  matB   <- CM.mulSparse matW1 matW2'
  CM.printSparse matB "B  "
  matC   <- CM.addSparse matA matB
  CM.printSparse matC "C  "
  CM.getNNZ matC
--   CM.addSparse matB matA >>= CM.getNNZ
--   CM.getNNZ matB
--   CM.getNNZ matW1


main = do
  -- Some testing files:
  ee <- CCS.fromFile "/home/patrick/Dropbox/phd/data/eit625_A.mtx"
  xx <- CCS.fromFile "/home/patrick/Dropbox/phd/data/eit625_W1.mtx"
  yy <- CCS.fromFile "/home/patrick/Dropbox/phd/data/eit625_W2.mtx"
  let cn = 5
      ww = ee `add` transpose ee
      zz = xx `multiply` transpose yy
      -- Indices that are {unsorted, sorted, missing-a-column}:
      ci = Vec.fromList [0,4,1,2,2,3,1,0,3,1,4,1,0,4,4,1] :: IVec
      cj = Vec.fromList [0,0,0,1,1,1,1,1,2,2,3,3,4,4,4,4] :: IVec
      ck = Vec.fromList [0,0,0,1,1,1,1,1,3,3,3,3,4,4,4,4] :: IVec
      -- Assemble a CCS sparse matrix:
      ri = Vec.fromList [0,1,3,0,1,2,3,4,1,2,3,4,0,1,3,4] :: IVec
      cp = packCols cn ci :: IVec
      xs = Vec.fromList [1..16] :: DVec
      mm = CCS cn ri cp xs :: CCSD
      -- Assemble another sparse matrix:
      rj = Vec.fromList [3,0,2,4,1,0,3,2,3,1,2,4,2,1,3,4] :: IVec
      cq = packCols cn ck :: IVec
      ys = Vec.fromList ([-6,-5..(-1)] ++ [2,4..20]) :: DVec
      aa = CCS cn rj cq ys :: CCSD
      -- And some sparse-vectors:
      ui = Vec.fromList [17,1,23,2,15,0,18,11,13,4] :: IVec
      ux = Vec.fromList [1..10] :: DVec
      uu = SparseVector 30 ui ux
      vi = Vec.fromList [29,6,14,7,23,9,21,22,18,0] :: IVec
      vx = Vec.fromList [5,7..23] :: DVec
      vv = SparseVector 30 vi vx
--   CM.evalCholMod cmTest >>= print
--   print . getNNZ $ ww `add` zz

  Just cm <- CM.beginCholMod
  (mX, lX, bX, fX, eX, zX) <- flip CM.runCholModT cm $ do
    mX <- CM.readSparse "/home/patrick/Dropbox/phd/data/eit625_A.mtx"
    CM.printSparse mX "A"
    lX <- CM.analyse mX
    CM.printFactor lX "L"
    gX <- CM.readSparse "/home/patrick/Dropbox/phd/data/eit625_fs.mtx"
    fX <- CM.submatrix gX (Vec.enumFromN 0 625) (Vec.singleton 0)
--     fX <- CM.submatrix gX (Vec.enumFromN 0 625) (Vec.fromList [0,1])
    CM.printSparse fX "f"
    bX <- CM.sparseToDense fX
    CM.printDense bX "b"
    eX <- ccs2cmsD ee
    CM.printSparse eX "E"
    zX <- ccs2cmsD zz
    CM.printSparse zX "Z"
    mX `seq` lX `seq` bX `seq` fX `seq` return (mX, lX, bX, fX, eX, zX)

  let pacbench =
        bgroup "Packing"
        [ bench "packCols           " $ whnf (Vec.sum . packCols       cn) ci
        , bench "packCols   (sorted)" $ whnf (Vec.sum . packCols       cn) cj
        , bench "unsafePack (sorted)" $ whnf (Vec.sum . unsafePackCols cn) cj
        , bench "unscanlSum (sorted)" $ whnf (Vec.sum . unscanlSum) cp
        , bench "i2p        (sorted)" $ whnf (Vec.sum . i2p'           cn) cj
        , bench "packCols     (holy)" $ whnf (Vec.sum . packCols       cn) ck
        , bench "unsafePack   (holy)" $ whnf (Vec.sum . unsafePackCols cn) ck
        , bench "unscanlSum   (holy)" $ whnf (Vec.sum . unscanlSum) cq
        , bench "i2p          (holy)" $ whnf (Vec.sum . i2p'           cn) ck
        ]
      unpbench =
        bgroup "Unpacking"
        [ bench "unpack             " $ whnf (Vec.sum . unpack         cn) cq
        , bench "p2i                " $ whnf (Vec.sum . p2i              ) cq
        , bench "p2j                " $ whnf (Vec.sum . p2j              ) cq
        , bench "full               " $ whnf (Vec.sum . full             ) mm
        ]
      nulbench = bgroup "Null" []

  defaultMain
    [ nulbench
    , pacbench
    , unpbench
--     , matbench ee cm mX
--     , matbench zz
--     , matbench mm
--     , linbench cm mX lX bX fX
--     , mulbench ee zz cm eX zX
--     , mulbench aa mm
--     , addbench ee zz cm eX zX
--     , addbench aa mm
--     , vecbench (vecSort uu) (vecSort vv)
--     , vecbench uu vv
    ]
